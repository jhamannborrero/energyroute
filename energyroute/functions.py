#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 14:25:19 2020

@author: jehamann
"""
from scipy.signal import find_peaks
import numpy as np
from chargecurve import AutoCharge, AutoDrive
import datetime
import pandas as pd
import scipy.constants as ct


##############
## Functions ##
##############
        
#collect all data for each charging curve at store it as AutoCharge() objects
def collect_charges(df, peaks_id):
    """
    Usage: collect_charges(df, peaks_id)
    
    df: Pandas DataFrame with the battery charging data.
    peak_id: List with the index positions used to determine start and 
    end of charging event
 
    Returns a list where each element is an AutoCharge() object.        
    """
    
    #cols to collect
    cols =['hvBatteryCurrent','hvBatteryVoltage','hvBatteryTemp','hvSocActualDisplay', 
           'hvSocActualBms','timestamp', 'latitude', 'longitude','model']

    #check that the peaks_id list contains even number of elements
    if len(peaks_id) % 2 == 0:
            
        charges = [AutoCharge(df.iloc[peaks_id[i]:peaks_id[i+1]][cols].\
                                  drop_duplicates(subset='hvSocActualDisplay', keep='last')) \
                       for i in np.arange(0,len(peaks_id),2)]
    else:

        charges = [AutoCharge(df.iloc[peaks_id[i]:peaks_id[i+1]][cols].\
                                  drop_duplicates(subset='hvSocActualDisplay', keep='last')) \
                       for i in np.arange(0,len(peaks_id[1:]),2)]
        
        
    #remove charges that take longer than 5 Hours, i.e., wrongly estimated
    for i, v in enumerate(charges):
        if v.charging_time > 5:
            charges.pop(i)
    
    return charges

#collect all data for each drive and stores it as AutoDrive() objects
def collect_drives(df):
    """
    Usage: collect_drives(df, peaks_id)
    
    df: Pandas DataFrame with the drive data.
    peak_id: List with the index positions used to determine start and 
    end of drive event
 
    Returns a list where each element is an AutoDrive() object.        
    """
    # add timedeltas
    dff = df.copy() 
    dff['timedelta'] = dff.timestamp.diff(1)
    # if time delta is larger than 10 mins, this will be considered aonther ride
    peaks_idx = dff[dff.timedelta > datetime.timedelta(seconds=600)] 
    # add initial index value and sort index
    peaks_idx = peaks_idx.append([dff.iloc[0]]).sort_index()
    peaks_idx = peaks_idx.index.values

    drives = [AutoDrive(dff.loc[peaks_idx[i]:peaks_idx[i+1]][:-1]) \
              for i in range(len(peaks_idx[:-1]))]

    #remove drives that are too short (less than 4km) or too large (wrongly determined)
#    for d in drives:
#        if d.distance < 5 or d.distance > 400:
#            drives.remove(d)

    return drives

def get_peaks(df, col, kind='charge'):
    """
    Function to get index values where there are peaks in the data (maxima and minima)
    df: dataframe containing the data to a analize
    col (string): col name used to identify peaks 
    """
    
    if kind == 'charge':
        # get maxima
        max_id =find_peaks(df[col].values)[0]
        #get minima
        min_id =find_peaks(-1*df[col].values)[0]

    elif kind == 'drive':
        # get maxima
        max_id =find_peaks(df[col].values, prominence=2)[0]
        #get minima
        min_id =find_peaks(-1*df[col].values, prominence=2)[0]
        
    peaks_id = np.append(min_id, max_id)
    #add initial and final points
    peaks_id = np.append(peaks_id,[0,len(df)-1])
    peaks_id.sort()
    
    return peaks_id

def F_aero(speed, temp, area, c_drag, altitude, wind=0):
    """
    Calculates the aerodynamic force for the vehicle.
    
    Parameters:
    -----------
    speed : car speed/velocity in km/h 
    temp : Temperature in °C
    area : Frontal area of the vehicle in m^2
    c_drag : drag coefficient of the vehicle
    altitude : altitude in m
    wind : wind velocity in km/h
    
    Returns:
    --------
    This function returns a 'float' with the value of the erodynamic force in N
    """
    
    p_0 = 101325# Sea level standard atmospheric pressure Pa (N/m^2)
    M = 0.02896968 # Molar mass of dry air kg/mol
    
    #unit conversion
    T = temp + 273.15 # °C to K
    v = speed / 3.6 # km/h to m/s
    wind = wind / 3.6
    
    #barometric formula
    P_air = p_0*np.exp((-1 * M * ct.g * altitude)/(ct.R * T))
    
    # air density as function of Temp and Pressure
    rho_air = P_air * M / (ct.R * T)
    
    F = 0.5 * rho_air * area *c_drag * (v - wind)**2
    
    return F

def F_gra(mass , alpha):
    """
    Calculates the gravitational force for the vehicle.
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    alpha : road slope in radians
    
    Returns:
    --------
    This function returns a 'float' with the value of the gravitational force in N
    """
    
    F = mass * ct.g * np.sin(alpha) 
    
    return F

def F_fric(mass, alpha, wet=False):
    """
    Calculates the frictional force for the vehicle.
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    alpha : road slope in radians
    
    Returns:
    --------
    This function returns a 'float' with the value of the frictional force in N
    """
    if wet:
        mu = 0.013 # friction coefficient between wet Tires and wet asphalt
    else:
        mu = 0.011 # friction coefficient between dry Tires and dry asphalt
        
    F = mu * mass * ct.g * np.cos(alpha)
    
    return F

def F_acc(mass, acc):
    """
    Calculates the linear acceleration force for the vehicle.
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    acc : vehicle acceleration
    
    Returns:
    --------
    This function returns a 'float' with the value of the acceleration force in N
    """
    return mass * acc  

def P_ac(t_in, t_out):
    """
    Returns the power in kW required for the air condition 
    
    *****Needs to be improved*****
    """
    return abs(t_in - t_out)*0.05

def E_height(h_start, h_end, mass):
    """
    Returns the energy in kWh required to bring the car from a height h_start
    to a height h_end 
    """
    e = mass * ct.g * (h_end - h_start)/3.6e6 
    return e

def E_heating_ini(T_ini, T_des, vol, altitude):
    """
    Calculates the aerodynamic force for the vehicle.
    
    Parameters:
    -----------
    T_ini : Initial air Temperature inside the car in °C
    T_des : Desired Temperature in °C
    vol : Air volume inside the car to be heated in m^3
    altitude : altitude in m
    
    Returns:
    --------
    This function returns a 'float' with the value of the Energy required to heat
    the air inside the car from T_ini to T_des in kWh
    """
    
    #unit conversion
    T_ini = T_ini + 273.15 # °C to K
    T_des = T_des + 273.15 # °C to K
    
    c_p = 1004.685 # Constant-pressure specific heat of air W.s/(kg.K)
    p_0 = 101325# Sea level standard atmospheric pressure Pa (N/m^2)
    M = 0.02896968 # Molar mass of dry air kg/mol
    
    #barometric formula
    P_air = p_0*np.exp((-1 * M * ct.g * altitude)/(ct.R * T_ini))
    
    # air density as function of Temp and Pressure
    rho_air = P_air * M / (ct.R * T_ini)
    
    e = rho_air * c_p * vol * (T_ini - T_des)/3.6e6 
    return abs(e)

def P_heating(T_in, T_out, T_target):
    """
    Calculates the power required to heat the air cabin of an auto
    
    Parameter:
    ----------
    T_in: Temperature inside the car (°C)
    T_out: Temperature outside the car (°C)
    T_target: Target temperature inside the car (°C)
    
    Returns:
    --------
    Power in kW    
    """
    t_in = 273.15 + T_in
    t_out = 273.15 + T_out
    t_target = 273.15 + T_target

    p = 0.2*abs(t_in-t_target) + 0.02*abs(t_in-t_out)
    
    return p


def autoEnergy_v1(auto, add_weight, distance, t_in, t_out, h_start, h_end, soc_start, chargePointkW, wind):
    """
    """
    #
    Eff = 0.9 # motor efficiency
    
    vol = 5
    
    velocities = [vel for vel in range(10,200,10)]
    
    mass = auto.emptyWeight[0] + add_weight
    
    alpha = np.arcsin((h_end - h_start)/(distance*1000)) # overall drive slope
    
    Energy = pd.DataFrame({'Velocity [km/h]':[vel for vel in velocities],
                        'P aero [kW]':[F_aero(vel,t_out,auto.area[0], auto.dragFactor[0],h_start,wind) * (vel/3.6)/1000
                                       for vel in velocities],
                        'P fric [kW]':[F_fric(mass, alpha)* (vel/3.6)/1000 
                                       for vel in velocities],
                        'P gravity [kW]':[F_gra(mass, alpha)* (vel/3.6)/1000
                                          for vel in velocities],
                       })
    
    Energy['P electr [kW]'] = P_ac(t_in,t_out)

#    Energy['P height [kW]'] = (Energy['Velocity [km/h]']/distance)*E_height(h_start, h_end, mass)
#    Energy['P heat_ini [kW]'] = (Energy['Velocity [km/h]']/distance)*E_heating_ini(t_out, t_in, vol, h_start)
    Energy['P heat_ini [kW]'] = E_heating_ini(t_out, t_in, vol, h_start)*60/6

    # still need to add efficiency of the motor/battery (wirkungsgrad)
    Energy['E consumption [kWh/100km]'] = (Energy['P aero [kW]'] 
                                           + Energy['P fric [kW]'] 
                                           + Energy['P gravity [kW]'] 
                                           + Energy['P electr [kW]'] 
#                                           + Energy['P height [kW]']
                                           + Energy['P heat_ini [kW]']
                                          )*100/(Energy['Velocity [km/h]']*Eff)
                                           
    Energy['Drive range [km]'] = auto.nettoKwh[0]*100/Energy['E consumption [kWh/100km]']
    
    Energy['E demand trip [kWh]'] = Energy['E consumption [kWh/100km]']*distance/100
    
    Energy['E charge [kWh]'] = Energy['E demand trip [kWh]'].apply(
        lambda x: x - (auto.nettoKwh[0] * soc_start/100) + auto.nettoKwh[0]/4 # add 1/4 of battery for safety
        if (auto.nettoKwh[0] * soc_start/100) < x 
        else 0)
    
    Energy['Charging Stops'] =  np.ceil(Energy['E charge [kWh]']/auto.nettoKwh[0])
    
    # need to add efficiency of the chargePoint (more energy/time needed as planned)
    Energy['Charging time [h]'] =(Energy['E charge [kWh]']/chargePointkW) + Energy['Charging Stops']*(5/60)*2 # add 5 min/charge stop

    Energy['Driving time [h]'] = distance/Energy['Velocity [km/h]']

    Energy['Total time [h]'] =  Energy['Driving time [h]'] + Energy['Charging time [h]']

    return Energy

def E_aero(speed, temp, area, c_drag, altitude, distance, wind=0):
    """
    Calculates the aerodynamic Energy for the vehicle.
    
    Parameters:
    -----------
    speed : car speed/velocity in km/h 
    temp : Temperature in °C
    area : Frontal area of the vehicle in m^2
    c_drag : drag coefficient of the vehicle
    altitude : altitude in m
    wind : wind velocity in km/h
    distance : Total drive distance (m)
    
    Returns:
    --------
    This function returns a 'float' with the value of the erodynamic Energy in kWh
    """
    
    p_0 = 101325# Sea level standard atmospheric pressure Pa (N/m^2)
    M = 0.02896968 # Molar mass of dry air kg/mol
    
    #unit conversion
    T = temp + 273.15 # °C to K
    v = speed / 3.6 # km/h to m/s
    wind = wind / 3.6
    
    #barometric formula
    P_air = p_0*np.exp((-1 * M * ct.g * altitude)/(ct.R * T))
    
    # air density as function of Temp and Pressure
    rho_air = P_air * M / (ct.R * T)
    
    E = (0.5 * rho_air * area *c_drag * (v - wind)**2 * distance)/3.6e6
    
    return E


def E_fric(mass, distance, h_start, h_end, wet=False):
    """
    Calculates the frictional Energy for the vehicle.
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    distance : Total drive distance (m)
    h_start : altitude at initial location (m)
    h_end :  altitude at final location (m)
    wet : boolean, tell if the road is wet. Default is False
    
    Returns:
    --------
    This function returns a 'float' with the value of the frictional Energy in kWh
    """
    if wet:
        mu = 0.013 # friction coefficient between wet Tires and wet asphalt
    else:
        mu = 0.011 # friction coefficient between dry Tires and dry asphalt
        
    E = (mu * mass * ct.g * np.sqrt(distance**2 - (h_end - h_start)**2))/3.6e6
    
    return E

def E_grav(h_start, h_end, mass):
    """
    Returns the energy in kWh required to bring the car from a height h_start
    to a height h_end 
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    h_start : altitude at initial location (m)
    h_end :  altitude at final location (m)
    
    Returns:
    --------
    This function returns a 'float' with the value of the grav Energy in kWh
    """
    
    e = mass * ct.g * (h_end - h_start)/3.6e6 
    return e

def autoEnergy(auto, add_weight, distance, t_in, t_out, h_start, h_end, soc_start, chargePointkW, wind):
    """
    """
    #
    Eff = 0.9 # motor efficiency
    
    velocities = [vel for vel in range(10,210,10)]
    
    mass = auto.emptyWeight[0] + add_weight
    
    Energy = pd.DataFrame({'Velocity [km/h]':[vel for vel in velocities],
                           'E aero [kWh/100km]':[E_aero(vel,t_out,auto.area[0], auto.dragFactor[0], h_start,
                                                     distance, wind) * 100/(distance/1000)
                                                 for vel in velocities],
                           'E fric [kWh/100km]':[E_fric(mass, distance, h_start, h_end) * 100/(distance/1000)
                                                 for vel in velocities],
                           'E gravity [kWh/100km]':[E_grav(h_start, h_end, mass) * 100/(distance/1000)
                                                    for vel in velocities],
                           # here it is assumed that the car cold/warm at the beginning at the target T
                           'E heating [kWh/100km]': [P_heating(t_out, t_out, t_in) * 100/vel 
                                                     for vel in velocities], 
                           'E electric [kWh/100km]': [0.5 * 100/vel 
                                                      for vel in velocities],
                           })
    

    # still need to add efficiency of the motor/battery (wirkungsgrad)
    Energy['E Total [kWh/100km]'] = (Energy['E aero [kWh/100km]'] 
                                           + Energy['E fric [kWh/100km]'] 
                                           + Energy['E gravity [kWh/100km]'] 
                                           + Energy['E heating [kWh/100km]'] 
                                           + Energy['E electric [kWh/100km]']
                                          )/Eff
    
    Energy['Range [km]'] = auto.nettoKwh[0]*100/Energy['E Total [kWh/100km]']
    
    Energy['E demand trip [kWh]'] = Energy['E Total [kWh/100km]']*(distance/1000)/100
    
    Energy['E charge [kWh]'] = Energy['E demand trip [kWh]'].apply(
        lambda x: x - (auto.nettoKwh[0] * soc_start/100) + auto.nettoKwh[0]/4 # add 1/4 of battery for safety
        if (auto.nettoKwh[0] * soc_start/100) < x 
        else 0)
    
    Energy['Charging Stops'] =  np.ceil(Energy['E charge [kWh]']/auto.nettoKwh[0])
    
    # need to add efficiency of the chargePoint (more energy/time needed as planned)
    Energy['Charging time [h]'] =(Energy['E charge [kWh]']/chargePointkW) + Energy['Charging Stops']*(5/60)*2 # add 5 min/charge stop

    Energy['Driving time [h]'] = (distance/1000)/Energy['Velocity [km/h]']

    Energy['Total time [h]'] =  Energy['Driving time [h]'] + Energy['Charging time [h]']

    return Energy